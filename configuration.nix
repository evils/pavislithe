# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ 
    ./hardware-configuration.nix

    ./users.nix
    ./programs.nix
    ./print-scan.nix
    ./i18n.nix
    ./eaton.nix
    ./bluetooth.nix
    ./influx.nix

    ./wifi-login.nix
  ];

  # save some time on rebuilds...
  documentation.enable = false;
  documentation.man.enable = false;

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    initrd.kernelModules = [
      "spi_bcm2835"
      "spidev"
      # camera support
      "bcm2835-v4l2"
      # early boot messages
      "vc4"
      "bcm2835_dma"
      "i2c_bcm2835"
    ];
    kernelParams = [
      # for serial console
      "console=ttyS1,115200n8"
      # for virtual console
      "cma=32M"
    ];
    loader = {
      grub.enable = false;
      raspberryPi = {
        enable = true;
        version = 3;
        uboot.enable = true;
        # These two parameters are the important ones to get the
        # camera working. These will be appended to /boot/config.txt.
        # /boot/config.txt isn't used, manually edit config.txt in the FIRMWARE partition
        firmwareConfig = ''
          gpu_mem=16
          dtparam=audio=on
        '';
      };
      #generic-extlinux-compatible.enable = true;
    };
    extraModprobeConfig = ''
        options cf680211 ieee80211_regdom="US"
      '';
  };


  hardware.enableRedistributableFirmware = true;
  hardware.firmware = [ pkgs.wireless-regdb ];

  hardware.rasdaemon.enable = true;

  networking = {
    hostName = "pavislithe";
    useDHCP = false; # true is deprecated but still the default, set per interface
    interfaces.eth0.useDHCP = true;
    interfaces.wlan0.useDHCP = true;
    networkmanager.wifi.backend = "iwd";
    wireless = {
      enable = false;
      iwd.enable = true;
    };
    usePredictableInterfaceNames = true;
  };
  systemd.services.iwd.serviceConfig.Restart = "always";

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  # firmwareConfig for audio in the boot block above

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.autossh.sessions = [{
    name = "evils";
    user = "evils";
    monitoringPort = 0;
    extraArguments = "-NR 55502:localhost:22 pi@evils.eu -p 2916";
  }];

  services.tuptime.enable = true;

  services.clarissa = {
    enable = true;
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "21.05"; # Did you read the comment?

}

