{ config, lib, pkgs, ...}: {

  power.ups = {
    enable = false;
    ups.eaton = {
      description = "eaton ellipse pro 1600 iec";
      summary = "eaton via hsg";
      driver = "usbhid-ups";
      port = "auto";
    };
  };

  # i think this is my eaton...
  services.udev.extraRules = ''
    SUBSYSTEM=="hidraw", SUBSYSTEMS=="usb", ATTRS{idVendor}=="7793", ATTRS{idProduct}=="5911", MODE="0666"
  '';

}
