{ config, lib, pkgs, ...}: {

  fileSystems."/mnt/influxdb".options = [ "compress=zstd" ];

  networking.firewall.allowedTCPPorts = [
    # influxdb
    8086
    8088
    # grafana
    3000
  ];

  environment.systemPackages = with pkgs; [
    # for the influx CLI tool
    influxdb
  ];

  services.influxdb = {
    enable = true;
    # this module doesn't support influxdb 2.0 yet
    # changing the config input to via INFLUXDB_CONFIG_PATH results in an unrelated error?
    #package = pkgs.influxdb2;
    dataDir = "/mnt/influxdb/influx/db";
    extraConfig = {
      # TODO figure out certificates
      http = {
        auth-enabled = true;
        https-enabled = true;
        https-certificate = "/mnt/influxdb/certs/influxdb-selfsigned.crt";
        https-private-key = "/mnt/influxdb/certs/influxdb-selfsigned.key";
      };
      collectd = [{
        enable = true;
        bind-address = ":25826";
        database = "10.30.2.0";
        #typesdb = /mnt/influxdb/types.db";
      }];
    };
  };

  services.telegraf = {
    enable = true;
    extraConfig = {
      agent = {
        interval = "10s";
      };
      inputs = {
        cpu = [{
          percpu = true;
          totalcpu = true;
          collect_cpu_time = false;
          report_active = false;
        }];
        disk = [{
          ignore_fs = [ "tmpfs" "devtmpfs" "devfs" "overlay" "aufs" "squashfs" ];
        }];
        mqtt_consumer = [{
          servers = [ "tcp://mqtt:1883" ];
          topics = [ "temperature" ];
          data_format = "value";
          data_type = "float";
        }];
      };
      outputs = {
        influxdb = [{
          urls = [ "https://localhost:8086" ];
          database = "tbd";
          username = "tbd";
          password = "tbd";
          insecure_skip_verify = true;
          namepass = [ "clarissa" "pam" "mqtt_consumer" ];
        }];
      };
    };
  };


  services.grafana = {
    enable = true;
    dataDir = "/mnt/influxdb/grafana";
    addr = "";
    certFile = "/mnt/influxdb/certs/influxdb-selfsigned.crt";
    certKey = "/mnt/influxdb/certs/influxdb-selfsigned.key";
  };

}
