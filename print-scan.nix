{ config, lib, pkgs, ...}: {

  services.avahi = {
    enable = true;
    publish = {
      enable = true;
      userServices = true;
    };
  };
  networking.firewall.allowedUDPPorts = [ 631 ];
  networking.firewall.allowedTCPPorts = [ 631 ];


  hardware.sane = {
    enable = true;
    extraBackends = [ pkgs.hplip ];
  };
  services.saned = {
    enable = true;
    extraConfig = ''
      10.0.0.0/8
      192.168.0.0/16
    '';
  };

  services.printing = {
    enable = true;
    drivers = [ pkgs.hplip ];
    browsing = true;
    listenAddresses = [ "*:631" ];
    allowFrom = [ "all" ];
    defaultShared = true;
  };
}
