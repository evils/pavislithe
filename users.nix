{ config, lib, pkgs, ...}: {

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.evils = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "audio" "video" "docker" "dialout" "input" "beep" "scanner" "lp" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBj6N5zlcXIg5RzmTFovzGU3a80LvXUmnkBbIT29HuoS evils@valix"
    ];
  };

}
